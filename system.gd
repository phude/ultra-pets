extends Node2D

class Player:
	var display_name
	var gold = 0
	var tier = 1
	var tier_up_cost
	var shop_slots = 4
	var shop_lineup = []
	var points = 0
	
	func _init(display_name):
		self.display_name = display_name
		self.tier_up_cost = 5


var g_players = []
var g_deck = []


var p_proto = {
	"item": {
		"apple": {
			"cost": 2,
			"effect": "+3h",
		}
	},
	"creature": {
		"zombie": {
			"name": "Zombie",
			"attack": 5,
			"health": 3,
		},
	},
}

func init(num_players):
	for i in range(num_players):
		g_players.append(Player.new("player " + str(i)))


func roll_shop(player_id):
	var max_tier = player_id.tier
	var num_slots = player_id.shop_slots

func do_round_start():
	for player in g_players:
		player.gold = 10
		roll_shop()
